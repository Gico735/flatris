FROM node

RUN mkdir /app

EXPOSE 3000
# WORKDIR /app

COPY package.json /app
RUN cd /app && yarn install

COPY . /app


# RUN yarn test
RUN cd /app &&  yarn build


CMD cd /app && yarn start